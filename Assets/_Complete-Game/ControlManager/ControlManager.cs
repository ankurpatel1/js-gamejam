﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager : MonoBehaviour
{
    public static ControlManager Instance;

    [System.Serializable]
    public class ControllerSetting
    {
        public int Player;
        public bool KeyMouse;
        public int ControllerIndex;
    }

    public class Control
    {
        public bool Fire;
        public Vector3 MousePosition;
        public float MoveXAxis;
        public float MoveYAxis;
        public float AimXAxis;
        public float AimYAxis;
    }

    public ControllerSetting[] Settings;

    public void Awake()
    {
        Instance = this;
    }

    public ControllerSetting GetControlSetting( int player )
    {
        if(Settings != null && Settings.Length > 0)
        {
            return System.Array.Find(Settings, s => s.Player == player);
        }

        return null;
    }

    // Update is called once per frame
    public Control GetPlayerControl(int player)
    {
        ControllerSetting setting = GetControlSetting(player);

        if(setting == null)
            return null;

        Control control = new Control();

        if(setting.KeyMouse)
        {
            control.Fire = Input.GetMouseButtonDown(0);
            control.MoveXAxis = Input.GetAxis("Horizontal");
            control.MoveYAxis = Input.GetAxis("Vertical");
            control.MousePosition = Input.mousePosition;
        }

        control.Fire = control.Fire || Input.GetButton("JS" + setting.ControllerIndex + "Fire");
        control.MoveXAxis += Input.GetAxis("JS" + setting.ControllerIndex + "LXAxis");
        control.MoveYAxis += Input.GetAxis("JS" + setting.ControllerIndex + "LYAxis");
        control.AimXAxis += Input.GetAxis("JS" + setting.ControllerIndex + "RXAxis");
        control.AimYAxis += Input.GetAxis("JS" + setting.ControllerIndex + "RYAxis");

        return control;
    }

    public Control GetPlayerControl()
    {
        Control control = new Control();

        control.Fire = Input.GetMouseButtonDown(0);
        control.MoveXAxis = Input.GetAxis("Horizontal");
        control.MoveYAxis = Input.GetAxis("Vertical");
        control.MousePosition = Input.mousePosition;

        control.Fire = control.Fire || Input.GetButton("JS1Fire");
        control.MoveXAxis += Input.GetAxis("JS1LXAxis");
        control.MoveYAxis += Input.GetAxis("JS1LYAxis");
        control.AimXAxis += Input.GetAxis("JS1RXAxis");
        control.AimYAxis += Input.GetAxis("JS1RYAxis");

        return control;
    }
}
