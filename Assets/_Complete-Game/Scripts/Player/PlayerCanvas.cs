﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CompleteProject {
    public class PlayerCanvas : MonoBehaviour
    {
        public Slider _HealthSlider;
        public Text _ScoreText;
        public Slider _BulletSlider;
        public Text _PlayerName;

        public void SetSliderValue(int value)
        {
            _HealthSlider.value = value;
        }

        public void SetScore(string text)
        {
            _ScoreText.text = text;
        }

        public void SetBulletvalue(float value)
        {
            _BulletSlider.value = value;
        }

        private void Update()
        {
            if (_BulletSlider != null)
            {
                _BulletSlider.value += GameManager.pInstance._BulletRegenRate;
            }
        }
    }
}