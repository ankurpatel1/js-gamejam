﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
using Photon.Pun;

namespace CompleteProject
{
    public class PlayerShooting : MonoBehaviour
    {
        public Player player;
        private int damagePerShot = 5;                  // The damage inflicted by each bullet.
        public float timeBetweenBullets = 0.15f;        // The time between each shot.
        public float range = 100f;                      // The distance the gun can fire.


        float timer;                                    // A timer to determine when to fire.
        Ray shootRay = new Ray();                       // A ray from the gun end forwards.
        RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
        int shootableMask;                              // A layer mask so the raycast only hits things on the shootable layer.
        ParticleSystem gunParticles;                    // Reference to the particle system.
        LineRenderer gunLine;                           // Reference to the line renderer.
        AudioSource gunAudio;                           // Reference to the audio source.
        Light gunLight;                                 // Reference to the light component.
		public Light faceLight;								// Duh
        float effectsDisplayTime = 0.2f;                // The proportion of the timeBetweenBullets that the effects will display for.

        public string _FireButtonName;
        private PhotonView mPhotonView;

        void Awake ()
        {
            // Create a layer mask for the Shootable layer.
            shootableMask = LayerMask.GetMask ("Shootable");

            // Set up the references.
            gunParticles = GetComponent<ParticleSystem> ();
            gunLine = GetComponent <LineRenderer> ();
            gunAudio = GetComponent<AudioSource> ();
            gunLight = GetComponent<Light> ();
			//faceLight = GetComponentInChildren<Light> ();
        }

        private void Start()
        {
            mPhotonView = transform.parent.gameObject.GetComponent<PhotonView>();

        }

        void Update ()
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;
            if (!GameManager.pInstance.pIsMultiplayer || mPhotonView.IsMine)
            {
#if !MOBILE_INPUT

                bool fire = false;
                if (ControlManager.Instance != null && player != null)
                {
                    ControlManager.Control control = GameManager.pInstance.pIsMultiplayer ? ControlManager.Instance.GetPlayerControl() : ControlManager.Instance.GetPlayerControl(player.pPlayerIndex);
                    fire = control.Fire;
                }

                if (fire && timer >= timeBetweenBullets && Time.timeScale != 0)
                {
                    // ... shoot the gun.
                    Shoot();
                }

#else
                // If there is input on the shoot direction stick and it's time to fire...
                if ((CrossPlatformInputManager.GetAxisRaw("Mouse X") != 0 || CrossPlatformInputManager.GetAxisRaw("Mouse Y") != 0) && timer >= timeBetweenBullets)
                {
                    // ... shoot the gun
                    Shoot();
                }
#endif
            }
            // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
            if(timer >= timeBetweenBullets * effectsDisplayTime)
            {
                // ... disable the effects.
                DisableEffects ();
            }
        }


        public void DisableEffects ()
        {
            // Disable the line renderer and the light.
            gunLine.enabled = false;
			faceLight.enabled = false;
            gunLight.enabled = false;
        }

        private Transform GetTarget()
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            Transform target = null;
            float distance = float.MaxValue;

            foreach(GameObject obj in players)
            {
                if(obj == player.gameObject)
                    continue;

                if(Vector3.Distance(player.transform.position, obj.transform.position) <= distance)
                {
                    target = obj.transform;
                    distance = Vector3.Distance(player.transform.position, obj.transform.position);
                }
            }

            return target;
        }

        void Shoot ()
        {
            if (player.pBulletCount >= 1)
            {
                ShowShootingEffect();
                player.pBulletCount--;
                player.UpdateBulletCount();
                // Set the shootRay so that it starts at the end of the gun and points forward from the barrel.
                shootRay.origin = transform.position;
                shootRay.direction = transform.forward;

           // Perform the raycast against gameobjects on the shootable layer and if it hits something...
                if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
                {
                    PlayerHealth opponentHealth = shootHit.collider.GetComponent<PlayerHealth>();
                    if (opponentHealth != null && !opponentHealth.pPlayer.pIsPlayerReSpawned)
                    {
                        if (GameManager.pInstance.pIsMultiplayer)
                        {
                            PhotonView view = shootHit.collider.GetComponent<PhotonView>();
                            if (view != null)
                                view.RPC("TakeDamage", RpcTarget.AllBuffered, damagePerShot, shootHit.point);

                            view = GetComponentInParent<PhotonView>();
                            if (view != null)
                                view.RPC("ShowShootEffect", RpcTarget.AllBuffered, shootHit.point);
                        }
                        else
                            opponentHealth.TakeDamage(damagePerShot, shootHit.point);
                    }

                    gunLine.SetPosition(1, shootHit.point);
                }
                else
                {
                    Vector3 pos = shootRay.origin + shootRay.direction * range;
                    gunLine.SetPosition(1, pos);

                    PhotonView view = GetComponentInParent<PhotonView>();
                    if (view != null)
                        view.RPC("ShowShootEffect", RpcTarget.AllBuffered, pos);
                }
            }
        }

        public void ShowShootingEffect()
        {
			Transform target = GetTarget();

            if(target == null)
                return;
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToTarget = target.position - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToTarget.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotatation = Quaternion.LookRotation(playerToTarget);

			player.transform.rotation = newRotatation;

            // Reset the timer.
            timer = 0f;

            // Play the gun shot audioclip.
            gunAudio.Play();

            // Enable the lights.
            gunLight.enabled = true;
            faceLight.enabled = true;

            // Stop the particles from playing if they were, then start the particles.
            gunParticles.Stop();
            gunParticles.Play();

            // Enable the line renderer and set it's first position to be the end of the gun.
            gunLine.enabled = true;
            gunLine.SetPosition(0, transform.position);
        }

        public void SetGunLinePos(Vector3 position)
        {
            gunLine.SetPosition(1, position);
        }

    }
}