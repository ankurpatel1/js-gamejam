﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CompleteProject
{
    public class Player : MonoBehaviour
    {
        public PlayerHealth pPlayerHealth { get; set; }

        public bool pIsDead { get; set; }

        private Vector3 mSpawnPoint;

        public int pPlayerIndex { get; set; }
        public int pCarriedGems { get; set; }
        private int mCollectedGems;

        private PlayerData mPlayerData;

        public SkinnedMeshRenderer[] _Renderer;
        private float mBlinkTimer;

        public bool pIsPlayerReSpawned { get; set; }

        public float pBulletCount { get; set; }

        private void Awake()
        {
            pPlayerHealth = gameObject.GetComponent<PlayerHealth>();
            mSpawnPoint = transform.position;
        }

        public void Init()
        {
            transform.position = mSpawnPoint;
            pIsDead = false;
            UpdateHealth(pPlayerHealth.startingHealth);
            pPlayerHealth.Init();
            _Renderer[0].material.SetTexture("_MainTex", mPlayerData._Texture);
            pBulletCount = GameManager.pInstance._BulletCount;
            mPlayerData._Canvas._BulletSlider.maxValue = pBulletCount;
            mPlayerData._Canvas._BulletSlider.value = pBulletCount;
            SetScore();

            if(CompleteProject.GameManager.pInstance.pIsMultiplayer)
            {
                Photon.Pun.PhotonView view = GetComponent<Photon.Pun.PhotonView>();

                if(view != null)
                    UpdatePlayerName(view.Owner.NickName);
            }
        }

        public void UpdateHealth(int value)
        {
            if (mPlayerData == null)
                mPlayerData = GameManager.pInstance._PlayerDataList.Find(x => x.pPlayer == this);
            if (mPlayerData != null)
                mPlayerData._Canvas.SetSliderValue(value);            
        }

        public void UpdatePlayerName(string name)
        {
            if (mPlayerData == null)
                mPlayerData = GameManager.pInstance._PlayerDataList.Find(x => x.pPlayer == this);
            if (mPlayerData != null)
                mPlayerData._Canvas._PlayerName.text = name;
        }

        public void UpdateBulletCount()
        {
            if (mPlayerData == null)
                mPlayerData = GameManager.pInstance._PlayerDataList.Find(x => x.pPlayer == this);
            if (mPlayerData != null)
                mPlayerData._Canvas.SetBulletvalue(pBulletCount);
        }

        public void Death()
        {
            pIsDead = true;
            StartCoroutine(GameManager.pInstance.SpawnGems(this));
            if (pCarriedGems > 0)
                pCarriedGems--;            
            GameManager.pInstance.RespawnPlayer(this);
            StartCoroutine(DeactivePlayer());
        }

        IEnumerator DeactivePlayer()
        {
            yield return new WaitForSeconds(1);
            gameObject.SetActive(false);
        }        

        private void OnCollisionEnter(Collision collision)
        {
            if (!pIsDead)
            {
                if (collision.gameObject.tag == "Gem")
                {
                    pCarriedGems++;
                    pPlayerHealth.PlayClip(GameManager.pInstance._GemsCollect);
                    SetScore();

                    if (pCarriedGems == GameManager.pInstance._ScoreToWin)
                        GameManager.pInstance._PauseMenu.ShowWin(this);

                    Destroy(collision.gameObject);
                    if (!GameManager.pInstance.pGemSpawning)
                        GameManager.pInstance.PlaceGem();
                }
                else if(collision.gameObject.tag == "Medical")
                {
                    pPlayerHealth.PlayClip(GameManager.pInstance._GemsCollect);
                    Destroy(collision.gameObject);
                    pPlayerHealth.currentHealth = (int)(pPlayerHealth.currentHealth * 1.5f);
                    UpdateHealth(pPlayerHealth.currentHealth);
                    if (!GameManager.pInstance.pGemSpawning)
                        GameManager.pInstance.PlaceGem();
                }
            }
           
        }

        public void SetScore()
        {
            if (mPlayerData == null)
                mPlayerData = GameManager.pInstance._PlayerDataList.Find(x => x.pPlayer == this);
            if (mPlayerData != null)
                mPlayerData._Canvas.SetScore("Score: " + pCarriedGems + "/" + GameManager.pInstance._ScoreToWin);
        }

        public IEnumerator StartBlinking()
        {
            if (mBlinkTimer <= GameManager.pInstance._PlayerBlinkTime)
            {
                foreach (SkinnedMeshRenderer rd in _Renderer)
                {
                    rd.enabled = !rd.enabled;
                }
                float delay = GameManager.pInstance._PlayerBlinkRate;
                yield return new WaitForSeconds(delay);
                mBlinkTimer += delay;
                StartCoroutine(StartBlinking());
            }
            else
            {
                foreach (SkinnedMeshRenderer rd in _Renderer)
                {
                    rd.enabled = true;
                }
                pIsPlayerReSpawned = false;
                mBlinkTimer = 0;               
            }
        }

        private void Update()
        {
            if (pBulletCount < GameManager.pInstance._BulletCount)
            {
                pBulletCount += GameManager.pInstance._BulletRegenRate;               
                UpdateBulletCount();
            }
        }
    }
}
