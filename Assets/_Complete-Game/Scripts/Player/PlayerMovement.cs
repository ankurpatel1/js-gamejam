﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
using Photon.Pun;

namespace CompleteProject
{
    public class PlayerMovement : MonoBehaviour
    {
        public bool AutoAim = true;
        public Vector3 LaserOffset = Vector3.zero;
        private Vector3 prevMousePosition = Vector3.zero;
        private int playerIndex;
        private Quaternion newRotation = Quaternion.identity;
        public float speed = 6f;            // The speed that the player will move at.

        Vector3 movement;                   // The vector to store the direction of the player's movement.
        Animator anim;                      // Reference to the animator component.
        Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
#if !MOBILE_INPUT
        int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
        float camRayLength = 100f;          // The length of the ray from the camera into the scene.
#endif
        private bool mPrevPlayerWalking = false; 
        public eControllType _ControllType;

        public enum eControllType
        {
            NONE,
            GENRIC,
            WASD,
            ARROW_KEY,
        }

        void Awake ()
        {
#if !MOBILE_INPUT
            // Create a layer mask for the floor layer.
            floorMask = LayerMask.GetMask ("Floor");
#endif
            newRotation = transform.rotation;
            // Set up references.
            anim = GetComponent <Animator> ();
            playerRigidbody = GetComponent <Rigidbody> ();

            if (!GameManager.pInstance.pIsMultiplayer)
            {
                Photon.Pun.PhotonView photonView = GetComponent<Photon.Pun.PhotonView>();
                if (photonView != null)
                    photonView.enabled = false;

                MultiplayerSetup setup = GetComponent<MultiplayerSetup>();
                if (setup != null)
                    setup.enabled = false;
            }
        }

        private void Start()
        {
            Player player = GetComponent<Player>();

            if(player != null)
                playerIndex = player.pPlayerIndex;
        }


        void FixedUpdate()
        {
            float h = 0, v = 0;
            // Store the input axes.

            if(ControlManager.Instance != null)
            {
                ControlManager.Control control = GameManager.pInstance.pIsMultiplayer ? ControlManager.Instance.GetPlayerControl() : ControlManager.Instance.GetPlayerControl(playerIndex);
                h = control.MoveXAxis;
                v = control.MoveYAxis;
            }
            else
            {
                if (_ControllType == eControllType.ARROW_KEY)
                {
                    h = CrossPlatformInputManager.GetAxisRaw("Horizontal_LR");
                    v = CrossPlatformInputManager.GetAxisRaw("Vertical_updown");
                }
                else if (_ControllType == eControllType.WASD)
                {
                    h = CrossPlatformInputManager.GetAxisRaw("Horizontal_AD");
                    v = CrossPlatformInputManager.GetAxisRaw("Vertical_WS");
                }
                else if (_ControllType == eControllType.GENRIC)
                {
                    h = CrossPlatformInputManager.GetAxisRaw("Horizontal");
                    v = CrossPlatformInputManager.GetAxisRaw("Vertical");
                }
            }

            // Move the player around the scene.
            Move(h, v);

            // Turn the player to face the mouse cursor.
            if(!AutoAim)
                Turning ();

            // Animate the player.
            Animating (h, v);
        }


        void Move (float h, float v)
        {
            // Set the movement vector based on the axis input.
            movement.Set (h, 0f, v);
            
            // Normalise the movement vector and make it proportional to the speed per second.
            movement = movement.normalized * speed * Time.deltaTime;
            // Move the player to it's current position plus the movement.
            transform.position += movement;
            //playerRigidbody.MovePosition (transform.position + movement);

           // ClampTransformHorizontallyToCamera(transform);
        }

        void ClampTransformHorizontallyToCamera(Transform playerTransform)
        {
            float leftBound = Camera.main.ViewportToWorldPoint(Vector3.zero).x;
            float rightBound = Camera.main.ViewportToWorldPoint(Vector3.right).x;
            float upBound= Camera.main.ViewportToWorldPoint(Vector3.up).y;
            float downBound = Camera.main.ViewportToWorldPoint(Vector3.down).y;
            Vector3 newPosition = playerTransform.position;
            newPosition.x = Mathf.Clamp(newPosition.x, leftBound, rightBound);
            newPosition.z = Mathf.Clamp(newPosition.z, downBound, upBound );
            playerTransform.position = newPosition;
        }

        void Turning ()
        {
            if (ControlManager.Instance != null)
            {
                ControlManager.Control control = ControlManager.Instance.GetPlayerControl(playerIndex);

                if(control.AimYAxis != 0 || control.AimXAxis != 0)
                {
                    newRotation = Quaternion.LookRotation(new Vector3(control.AimXAxis, 0, control.AimYAxis));
                    playerRigidbody.MoveRotation(Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime));
                    //playerRigidbody.MoveRotation(newRotatation);
                }

                if(control.MousePosition != Vector3.zero && control.MousePosition != prevMousePosition)
                {
                    prevMousePosition = control.MousePosition;
                    Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                    // Create a RaycastHit variable to store information about what was hit by the ray.
                    RaycastHit floorHit;

                    // Perform the raycast and if it hits something on the floor layer...
                    if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
                    {
                        // Create a vector from the player to the point on the floor the raycast from the mouse hit.
                        Vector3 playerToMouse = floorHit.point - transform.position;

                        // Ensure the vector is entirely along the floor plane.
                        playerToMouse.y = 0f;

                        // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
                        Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);

                        // Set the player's rotation to this new rotation.
                        playerRigidbody.MoveRotation(newRotatation);
                    }
                }
            }
        }


        void Animating (float h, float v)
        {
            // Create a boolean that is true if either of the input axes is non-zero.
            bool walking = h != 0f || v != 0f;
            // Tell the animator whether or not the player is walking.
            anim.SetBool ("IsWalking", walking);

            if (GameManager.pInstance.pIsMultiplayer && mPrevPlayerWalking != walking)
            {
                PhotonView view = GetComponent<PhotonView>();
                if (view != null)
                    view.RPC("UpdateMMOPlayerAnim", RpcTarget.AllBuffered, walking);
            }
            mPrevPlayerWalking = walking;
        }

        [PunRPC]
        void UpdateMMOPlayerAnim(bool walking)
        {
            PhotonView view = GetComponent<PhotonView>();
            if (view != null && !view.IsMine)
            {
                anim.SetBool("IsWalking", walking);
            }
        }
    }
}