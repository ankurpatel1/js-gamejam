﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

namespace CompleteProject
{
    public class MultiplayerSetup : MonoBehaviourPunCallbacks
    {
        private PhotonView mPhotonView;
        private Slider healthSlider;
        private bool mInit = false;
        

        private void Update()
        {
            if (!mInit && PhotonNetwork.IsConnected)
            {
                mInit = true;
                mPhotonView = GetComponent<PhotonView>();
                if (mPhotonView != null)
                    SetupPlayer();
            }
        }
        private void SetupPlayer()
        {
            if (!mPhotonView.IsMine)
            {
                int i = Photon.Pun.PhotonNetwork.IsMasterClient ? 1 : 0;

                Player p = GetComponent<Player>();
                p.pPlayerIndex = i + 1;
                GameManager.pInstance._PlayerDataList[i].pPlayer = p;
                p.Init();

                PlayerMovement movement = GetComponent<PlayerMovement>();
                if (movement != null)
                    movement.enabled = false;
            }
        }

        [PunRPC]
        public void TakeDamage(int Damage, Vector3 point)
        {
            PlayerHealth playerHealth = GetComponent<PlayerHealth>();

            // If the EnemyHealth component exist...
            if (playerHealth != null)
                playerHealth.TakeDamage(Damage, point);
        }

        [PunRPC]
        public void ShowShootEffect(Vector3 gunLinePos)
        {
            if (mPhotonView == null || mPhotonView.IsMine)
                return;

            PlayerShooting shooting = GetComponentInChildren<PlayerShooting>();

            if (shooting != null)
            {
                shooting.ShowShootingEffect();
                shooting.SetGunLinePos(gunLinePos);
            }
        }

        public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            UnityEngine.SceneManagement.SceneManager.LoadScene("LaunchGameUI");
        }
    }

}
