﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour {
	
	public AudioMixerSnapshot paused;
	public AudioMixerSnapshot unpaused;

    public GameObject _PausePanel;
    public GameObject _WinPanel;

    Canvas canvas;
	
	void Start()
	{
		canvas = GetComponent<Canvas>();
	}
	
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && !_WinPanel.activeInHierarchy)
		{
			canvas.enabled = !canvas.enabled;
            _PausePanel.SetActive(canvas.enabled);
			Pause();
		}
	}
	
	public void Pause()
	{
		Time.timeScale = Time.timeScale == 0 ? 1 : 0;
		Lowpass ();		
	}
	
	void Lowpass()
	{
		if (Time.timeScale == 0)
		{
			paused.TransitionTo(.01f);
		}
		
		else
			
		{
			unpaused.TransitionTo(.01f);
		}
	}

    public void ShowWin(CompleteProject.Player player)
    {
        Time.timeScale = 0;
        canvas.enabled = true;
        _WinPanel.SetActive(true);
        _PausePanel.SetActive(false);

        CompleteProject.PlayerData pData = CompleteProject.GameManager.pInstance._PlayerDataList.Find(d => d.pPlayer == player);

        if(pData != null)
        {
            string playerName = pData._Canvas._PlayerName.text;
            Text text = _WinPanel.GetComponentInChildren<Text>();

            if(text != null)
            {
                text.text = playerName + " WON";
            }
        }
    }

    public void Restart()
    {
        Time.timeScale = 1;
        Application.LoadLevel(0);
    }
	
	public void Quit()
	{
		#if UNITY_EDITOR 
		EditorApplication.isPlaying = false;
		#else 
		Application.Quit();
		#endif
	}
}
