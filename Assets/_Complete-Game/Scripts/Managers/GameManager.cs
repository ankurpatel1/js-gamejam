﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Photon.Pun;

namespace CompleteProject
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField]
        private float _RespawnTime;
        [SerializeField]
        private float _GemSpawnTime;
        [SerializeField]
        private GemCollector _GemPrefab;
        [SerializeField]
        private Player _PlayerPrefab;
        [SerializeField]
        private GameObject _MedicalPrefab;
        [SerializeField]
        private int _Radius;
        public string PlayerPrefName = "Player";

        [System.NonSerialized]
        public bool pIsMultiplayer;

        [System.NonSerialized]
        public bool pIsLocalPlayer;
        
        public int _ScoreToWin;
        public int _PlayerBlinkTime;
        public float _PlayerBlinkRate;
        public int _BulletCount;
        public float _BulletRegenRate;        
        
        public List<PlayerData> _PlayerDataList;

        public PauseManager _PauseMenu;

        public AudioClip _HurtClip;
        public AudioClip _DeathClip;
        public AudioClip _GemsCollect;

        public bool pGemSpawning { get; set; }      

        protected override void Awake()
        {
            base.Awake();

            if(PhotonNetwork.IsConnected)
               pIsMultiplayer = true;

            SpawnCharacter();
            PlaceGem();
            if (pIsMultiplayer)
                PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void PlaceGem()
        {
            StartCoroutine(SpawnGem());
        }

        void SpawnCharacter()
        {
            Player p;
            if (pIsMultiplayer)
            {
                int index = PhotonNetwork.IsMasterClient ? 0 : 1;
                GameObject player = PhotonNetwork.Instantiate(PlayerPrefName, _PlayerDataList[index].SpawnPos, Quaternion.identity);                
                p = player.GetComponent<Player>();
                p.transform.eulerAngles = _PlayerDataList[index]._Rotation;
                p.pPlayerIndex = index + 1;
                _PlayerDataList[index].pPlayer = p;
                p.Init();
            }
            else
            {
                for (int i = 0; i < _PlayerDataList.Count; i++)
                {
                    p = Instantiate(_PlayerPrefab, _PlayerDataList[i].SpawnPos, Quaternion.identity) as Player;
                    p.transform.eulerAngles = _PlayerDataList[i]._Rotation;
                    p.pPlayerIndex = i + 1;
                    _PlayerDataList[i].pPlayer = p;
                    p.Init();
                }
            }
        }

        public void RespawnPlayer(Player player)
        {
            StartCoroutine(Respawn(player));
        }

        IEnumerator Respawn(Player player)
        {
            yield return new WaitForSeconds(_RespawnTime);
            player.gameObject.SetActive(true);
            player.pIsPlayerReSpawned = true;
            StartCoroutine(player.StartBlinking());           
            player.Init();
        }

        public IEnumerator SpawnGem()
        {
            pGemSpawning = true;
            yield return new WaitForSeconds(_GemSpawnTime);
            pGemSpawning = false;
            if (IsMedicalAvailable())
                Instantiate(_MedicalPrefab, GetPositionAroundObject(Vector3.zero), Quaternion.identity);
            else
                Instantiate(_GemPrefab, GetPositionAroundObject(Vector3.zero), Quaternion.identity);
        }        

        public IEnumerator SpawnGems(Player player)
        {
            if (player.pCarriedGems > 0)
            {
                for (int i = 0; i < 1; i++)
                {
                    yield return new WaitForSeconds(0.5f);
                    Instantiate(_GemPrefab, GetPositionAroundObject(player.transform.position), Quaternion.identity);
                }
            }
        }

        public Vector3 GetPositionAroundObject(Vector3 position)
        {
            Vector3 offset = Random.insideUnitCircle * _Radius;
            Vector3 pos = position + offset;
            pos.y = 6;
            return pos;
        }

        public bool IsMedicalAvailable()
        {
            return Random.Range(0, 100) < 25;
        }
    }

    [Serializable]
    public class PlayerData
    {
        public Vector3 SpawnPos;
        public Vector3 _Rotation;
        public PlayerCanvas _Canvas;
        public Player pPlayer { get; set; }
        public Texture2D _Texture;
    }
}
