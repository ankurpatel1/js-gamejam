﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviour
{
    PhotonView mPhotonView;
	// Use this for initialization
	void Start ()
    {
        mPhotonView = GetComponent<PhotonView>();

        if(mPhotonView != null)
            SetupPlayer(mPhotonView.IsMine);
    }

    void SetupPlayer(bool isMe)
    {
        if(GameManager.pInstance != null && isMe)
        {
             Transform trans = PhotonNetwork.IsMasterClient ? GameManager.pInstance._OwnerPos : GameManager.pInstance._OpponentPos;
             if (trans != null)
             {
                 transform.position = trans.position;
                 transform.rotation = trans.rotation;
             }
        }
    }
}
