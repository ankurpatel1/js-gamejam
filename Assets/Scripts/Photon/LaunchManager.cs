﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public int _MaxPlayerAllowed;
    public string _GameScene;
    public GameObject _GameModePanel;
    public GameObject _EnterNamePanel;
    public GameObject _LoadingPanel;

	// Use this for initialization
	void Start ()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

	}

    public void SetPlayerName(string name)
    {
        if(string.IsNullOrEmpty(name) || name.Equals(PhotonNetwork.NickName))
           return;

        PhotonNetwork.NickName = name;
    }

    public void ShowNamePanel()
    {
        _GameModePanel.SetActive(false);
        _EnterNamePanel.SetActive(true);
    }

    public void JoinGame()
    {
        _EnterNamePanel.SetActive(false);
        _LoadingPanel.SetActive(true);
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
    }


    public void JoinLocalGame()
    {
        _GameModePanel.SetActive(false);
        _LoadingPanel.SetActive(true);
        SceneManager.LoadScene(_GameScene);
    }

    private void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = System.Convert.ToByte(_MaxPlayerAllowed);
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        string roomName = "FightZone_" + Random.Range(0, 1000);
        Debug.Log("roomName-" + roomName + "   playerNum- "+ roomOptions.MaxPlayers);
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    // Pun callbacks...

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("OnJoinedRoom called.");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        LoadGame();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        Debug.Log("OnJoinRandomFailed. Message is "+ message);
        CreateRoom();
    }

    public override void OnConnected()
    {
        base.OnConnected();
        Debug.Log("OnConnected...");

    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("OnConnectedToMaster...");
        PhotonNetwork.JoinRandomRoom();

    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("******* Please check your connection or try later");
        _EnterNamePanel.SetActive(true);
        _LoadingPanel.SetActive(false);
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("OnCreatedRoom...");
    }

    private void LoadGame()
    {
        if (!string.IsNullOrEmpty(_GameScene))
            PhotonNetwork.LoadLevel(_GameScene);
        else
            Debug.Log("Game could not be launched. _GameScene value is not set");
    }
}
