﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    public Transform _OwnerPos;
    public Transform _OpponentPos;
    public string _PlayerPrefabName;
    public List<Transform> _SpawnPositions;
    private static GameManager mInstance;
    public static GameManager pInstance
    {
        get { return mInstance; }
    }


    private void Awake()
    {
        if (mInstance == null)
            mInstance = this;
        else
            Destroy(gameObject);

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Start()
    {
        if(PhotonNetwork.IsConnected)
           SpawnPlayer();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("GameManager OnJoinedRoom");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("GameManager OnPlayerEnteredRoom "+ newPlayer.NickName);
    }

    private void SpawnPlayer()
    {
        if (!string.IsNullOrEmpty(_PlayerPrefabName))
        {
            int X = Random.Range(0,10);
            PhotonNetwork.Instantiate(_PlayerPrefabName, new Vector3(X, .5f, 4), Quaternion.identity);
            Debug.Log("Spawning Player... ");
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        Debug.Log("otherPlayer  " + otherPlayer);
    }
}
